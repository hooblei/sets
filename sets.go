
package sets


type Set map[interface{}]bool

func NewSet(values ...interface{}) Set {
    s := Set{}
    Add(s, values...)
    return s
}

// Adds the given value(s) to set `s`
func Add(s Set, values ...interface{}) {
    for _, v := range values {
        s[v] = true
    }
}

// Removes the given value(s) form set `s`
func Remove(s Set, values ...interface{}) {
    for _, v := range values {
        delete(s, v)
    }
}

// Returns the values of the given set `s` as a slice of `[]interface{}`
func Values(s Set) (vals []interface{}) {
    for v := range s {
        vals = append(vals, v)
    }

    return
}

func StringValues(s Set) (vals []string) {
    //vals := []string{}
    for v := range s {
        if sv, ok := v.(string); ok {
            vals = append(vals, sv)
        }
    }

    return
}

func IntValues(s Set) (vals []int) {
    //vals := []int{}
    for v := range s {
        if iv, ok := v.(int); ok {
            vals = append(vals, iv)
        }
    }

    return
}

// Unions the values of given sets into a new Set
func Union(sets ...Set) Set {
    union := Set{}
    for _, s := range sets {
        Add(union, Values(s)...)
    }

    return union
}

// Returns the difference of s1 and s2 as a new Set
func Diff(s1, s2 Set) Set {
    diff := Set{}
    for _, v := range Values(s1) {
        if !s2[v] {
            Add(diff, v)
        }
    }

    return diff
}

// Returns `true` if given Set `s` contains value `v`
func Contains(s Set, v interface{}) bool {
    return s[v]
}

// XXX: Todo ...
// func SymDiff(s1, s2 Set) Set {
// }

// Returns the intersection of s1 and s2 as a new Set
func Intersect(s1, s2 Set) Set {
    inter := Set{}
    if len(s2) > len(s1) {
        s1, s2 = s2, s1
    }

    for _, v := range Values(s1) {
        if s2[v] {
            Add(inter, v)
        }
    }

    return inter
}

