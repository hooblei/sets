
package sets

import (
    "testing"
)

var (
    vals = []interface{}{"a","bb",-123,""}
)

func chk(s Set, vals []interface{}, t *testing.T) {
    if len(s) != len(vals) {
        t.Errorf("Expected set lenth %d, got %d", len(vals), len(s))
    }

    for _, v := range vals {
        if !Contains(s, v) {
            t.Errorf("Missing expected set value %q", v)
        }
    }
}

func TestAddRemove(t *testing.T) {
    s1 := Set{}
    Add(s1, vals[0])
    Add(s1, vals[0], vals[1], vals[0], vals[3], vals[2])

    chk(s1, vals, t)

    Remove(s1, vals[0])
    chk(s1, vals[1:], t)

    for _, v := range vals[1:] {
        Remove(s1, v)
    }
    chk(s1, []interface{}{}, t)
    Add(s1, vals...)
    chk(s1, vals, t)
}

func TestUnion(t *testing.T) {
    a := Set{}
    Add(a, vals[:2]...)
    b := Set{}
    Add(b, vals...)
    chk(Union(a, b, NewSet(vals[0:2]...)), vals, t)
}

func TestDiff(t *testing.T) {
    a := Set{}
    Add(a, vals[0:2]...)
    b := Set{}
    Add(b, vals[1:3]...)
    chk(Diff(a, b), vals[0:1], t)
    chk(Diff(b, a), vals[2:3], t)
}

func TestIntersect(t *testing.T) {
    a := NewSet(vals[0:2]...)
    b := NewSet(vals[1:3]...)
    chk(Intersect(a, b), vals[1:2], t)
}

