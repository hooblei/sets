# sets

A bunch of utility functions to work on Sets\*

\*) `type Set map[interface{}]bool`

## Docs

See package documentation on [GoDoc](http://godoc.org/bitbucket.org/hooblei/sets)
